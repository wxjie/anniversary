$(document).ready(function() {
    loadData();
    blessBut();
});

var loadData = function () {
    var me = [];
    if($.cookie('xingzhi')){
        var arry = $.cookie("xingzhi").split(",");
        $(".page-warp-number").html(arry[0]);
        $(".page-warp-blessing input").prop("disabled",true).css("background-color","#FF9500").val("您送过祝福咯");
    }
        $.ajax({
            type:"post",
            url:"http://test.chunfeng.software/background/getallpoint",
            success:function (data) {
                callBackFunction(data,me);
            },
            dataType:"json"
        })

};

function callBackFunction(data,me)
{

    var geoCoord = "{";
    var datas = [];
    var x=200,y=0;
    var objs =data.Data;
    $(objs).each(function(index){
        var rand = parseInt(Math.random() * (x - y + 1) + y);
        var one="";
        if(index==(objs.length-1)){
            if($.cookie("xingzhi")){
            one= "\""+index+"\":["+this.Lng+","+this.Lat+"],\""+(index+1)+"\":["+$.cookie("xingzhi").split(",")[1]+","+$.cookie("xingzhi").split(",")[2]+"]";
            me.push(new function () {
                this.name = index+1;
                this.value = 300
            });}else {
                one= "\""+index+"\":["+this.Lng+","+this.Lat+"]";
            }
        }else {
            one = "\""+index+"\":["+this.Lng+","+this.Lat+"],";
        }
        datas.push(new function () {
            this.name=index;
            this.value = rand;
        });
        geoCoord +=one;
    });
    geoCoord +="}";
    var objosn = $.parseJSON(geoCoord);
    var myChart = echarts.init(document.getElementById('map'));
    myChart.setOption(option(datas,objosn,me));
}

var blessBut = function () {
    $(".page-warp-blessing input").on("click",function () {
        getAdd();
    })
};

var postMe = function (x,y) {
    var url = "http://test.chunfeng.software/background/addnewpoint?lng="+x+"&lat="+y;
    $.ajax({
        type:"post",
        url:url,
        success:function (data) {
            backObj(data);
        },
        dataType:"json"
    })
};
var backObj = function(data){
    if(data.Status == "ok"){
        $(".page-warp-number").html(data.Msg);
        $(".page-warp-blessing input").css("background-color","#FF9500").val("谢谢您的参与")
        $.cookie('xingzhi', data.Msg+","+data.Data.x+","+data.Data.y , { expires: 7 });
        loadData();
    }else {
        alert(data.Msg+"\n"+"请再次尝试");
        $(".page-warp-blessing input").prop("disabled",false).css("background-color","red");
    }

};
var getAdd = function () {
    $(".page-warp-blessing input").prop("disabled",true).css("background-color","#A6A6A6").val("正在定位！！");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(locationSuccess, locationError,{
            // 指示浏览器获取高精度的位置，默认为false
            enableHighAcuracy: true,
            // 指定获取地理位置的超时时间，默认不限时，单位为毫秒
            timeout:4000,
            // timeout: 5000,
            maximumAge:2000
            // 最长有效期，在重复获取地理位置时，此参数指定多久再次获取位置。
            // maximumAge: 3000
        });
    }else{
        postMe(-3333,-3333);
    }

};

var locationSuccess = function (position) {
    var coords = position.coords;
    postMe(coords.latitude,coords.longitude);
};
var locationError = function (error) {
    postMe(-3333,-3333);
};
var option = function (data,geoCoord,me) {
    var option = {
        dataRange: {
            show:false,
            min : 0,
            max : 500,
            calculable : true,
            color: ['maroon','purple','red','orange','yellow','lightgreen']
        },
        series : [
            {
                type: 'map',
                mapType: 'world',
                hoverable: false,
                roam:false,
                data : [],
                markPoint : {
                    symbolSize: 5,       // 标注大小，半宽（半径）参数，当图形为方向或菱形则总宽度为symbolSize * 2
                    itemStyle: {
                        normal: {
                            borderColor: '#87cefa',
                            borderWidth: 1,            // 标注边线线宽，单位px，默认为1
                            label: {
                                show: false
                            }
                        },
                        emphasis: {
                            borderColor: '#1e90ff',
                            borderWidth: 5,
                            label: {
                                show: false
                            }
                        }
                    },
                    data : data
                },
                geoCoord: geoCoord
            },
            {
                name: 'Top5',
                type: 'map',
                mapType: 'world',
                data:[],
                markPoint : {
                    symbol:'emptyCircle',
                    symbolSize : function (v){
                        return 10 + v/100
                    },
                    effect : {
                        show: true,
                        shadowBlur : 0
                    },
                    itemStyle:{
                        normal:{
                            label:{show:false}
                        }
                    },
                    data : me
                }
            }
        ]
    };
    return option;
};